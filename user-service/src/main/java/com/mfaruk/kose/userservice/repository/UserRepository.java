package com.mfaruk.kose.userservice.repository;

import com.mfaruk.kose.userservice.model.User;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CassandraRepository<User, String> {
    User findUserById(String id);
}
