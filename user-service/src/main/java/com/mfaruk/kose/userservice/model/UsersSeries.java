package com.mfaruk.kose.userservice.model;

import lombok.*;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@Data
@ToString
@Table(value = "users_series")
@EqualsAndHashCode(of = {"id"})
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsersSeries {
    @PrimaryKey
    @Column(value = "id")
    private String id = UUID.randomUUID().toString();

    @Column(value = "user_id")
    private String userId;

    @Column(value = "series_name")
    private String seriesId;
}
