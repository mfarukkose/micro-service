package com.mfaruk.kose.userservice.service;

import com.mfaruk.kose.userservice.dto.UserDto;
import com.mfaruk.kose.userservice.dto.UserSaveDto;
import com.mfaruk.kose.userservice.model.UsersSeries;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {

    List<UserDto> getAllUser(Pageable pageable);

    UserDto getUserById(String id) throws IllegalArgumentException;

    UserDto createUser(UserSaveDto user) throws Exception;

    UsersSeries saveSeriesToUser(UsersSeries usersSeries) throws Exception;

}
