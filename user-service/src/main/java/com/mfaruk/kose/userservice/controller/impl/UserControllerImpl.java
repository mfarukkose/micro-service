package com.mfaruk.kose.userservice.controller.impl;

import com.mfaruk.kose.userservice.controller.UserController;
import com.mfaruk.kose.userservice.dto.UserSaveDto;
import com.mfaruk.kose.userservice.model.UsersSeries;
import com.mfaruk.kose.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserControllerImpl implements UserController {
    @Autowired
    private UserService userService;

    @Override
    public ResponseEntity<Object> getAllUser(Pageable pageable) {
        try {
            return ResponseEntity.ok(userService.getAllUser(pageable));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

    @Override
    public ResponseEntity<Object> getUserById(String id) {
        try {
            return ResponseEntity.ok(userService.getUserById(id));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

    @Override
    public ResponseEntity<Object> createUser(UserSaveDto userSaveDto) {
        try {
            return ResponseEntity.ok(userService.createUser(userSaveDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

    @Override
    public ResponseEntity<Object> createUsersSeries(UsersSeries usersSeries) {
        try {
            return ResponseEntity.ok(userService.saveSeriesToUser(usersSeries));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }
}
