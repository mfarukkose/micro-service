package com.mfaruk.kose.userservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserSaveDto {

    private String name;
    private String surname;
    private String email;
    private String password;

}
