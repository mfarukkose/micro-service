package com.mfaruk.kose.userservice.repository;

import com.mfaruk.kose.userservice.model.UsersSeries;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersSeriesRepository extends CassandraRepository<UsersSeries, String> {
}
