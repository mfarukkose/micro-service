package com.mfaruk.kose.userservice.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.mfaruk.kose")
public class CustomBeanFactory {

    @Bean
    public ModelMapper getModelMapper () {
        return new ModelMapper();
    }
}
