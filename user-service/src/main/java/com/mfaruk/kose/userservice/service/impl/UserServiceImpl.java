package com.mfaruk.kose.userservice.service.impl;

import com.mfaruk.kose.feignservice.SeriesServiceClient;
import com.mfaruk.kose.feignservice.contract.SeriesDto;
import com.mfaruk.kose.userservice.dto.UserDto;
import com.mfaruk.kose.userservice.dto.UserSaveDto;
import com.mfaruk.kose.userservice.model.User;
import com.mfaruk.kose.userservice.model.UsersSeries;
import com.mfaruk.kose.userservice.repository.UserRepository;
import com.mfaruk.kose.userservice.repository.UsersSeriesRepository;
import com.mfaruk.kose.userservice.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    UsersSeriesRepository usersSeriesRepository;

    @Autowired
    SeriesServiceClient seriesServiceClient;


    @Override
    public List<UserDto> getAllUser(Pageable pageable) {
        return null;
    }

    @Override
    public UserDto getUserById(String id) throws IllegalArgumentException {
         User user = userRepository.findUserById(id);
         return modelMapper.map(user, UserDto.class);
    }

    @Transactional
    @Override
    public UserDto createUser(UserSaveDto userSaveDto) throws Exception {
        try {
            User user = modelMapper.map(userSaveDto, User.class);
            userRepository.save(user);
            return modelMapper.map(user, UserDto.class);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Transactional
    @Override
    public UsersSeries saveSeriesToUser(UsersSeries usersSeries) throws Exception {
        try {
            User user = userRepository.findById(usersSeries.getUserId()).orElseThrow(IllegalArgumentException::new);
            if(user != null) {
                Object seriesDto = seriesServiceClient.getSeriesById(usersSeries.getSeriesId());
                // linked hash map to json :D

                return usersSeriesRepository.save(usersSeries);
            } else {
                throw new Exception("Kullanıcı bulunmadı");
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

}
