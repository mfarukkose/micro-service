package com.mfaruk.kose.userservice.model;

import lombok.*;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Data
@ToString
@Table(value = "users")
@EqualsAndHashCode(of = {"id"})
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User implements Serializable {

    @PrimaryKey
    @Column(value = "id")
    private String id = UUID.randomUUID().toString();
    
    @Column(value = "name")
    private String name;

    @Column(value = "surname")
    private String surname;
    
    @Column(value = "email")
    private String email;
    
    @Column(value = "create_date")
    private Date createDate = new Date();
    
    @Column(value = "update_date")
    private Date updateDate;

    @Column(value = "password")
    private String password;

    @Column(value = "is_active")
    private boolean active = Boolean.TRUE;

}
