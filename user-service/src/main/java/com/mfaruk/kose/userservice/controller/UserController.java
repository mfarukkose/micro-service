package com.mfaruk.kose.userservice.controller;

import com.mfaruk.kose.userservice.dto.UserSaveDto;
import com.mfaruk.kose.userservice.model.UsersSeries;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface UserController {
    ResponseEntity<Object> getAllUser(@RequestBody Pageable pageable);

    @RequestMapping(value = "/getUserById", method = RequestMethod.GET)
    @ResponseBody
    ResponseEntity<Object> getUserById(@RequestParam("id") String id);

    @RequestMapping(value = "/createUser", method = RequestMethod.POST)
    @ResponseBody
    ResponseEntity<Object> createUser(@RequestBody UserSaveDto user);

    @RequestMapping(value = "/createUsersSeries", method = RequestMethod.POST)
    @ResponseBody
    ResponseEntity<Object> createUsersSeries(@RequestBody UsersSeries usersSeries);
}
