package com.mfaruk.kose.feignservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient("series-service")
public interface SeriesServiceClient {

    @RequestMapping(value = "/series/getSeriesById", method = RequestMethod.GET)
    @ResponseBody
    ResponseEntity<Object> getSeriesById(@RequestParam("id") String id);
}
