package com.mfaruk.kose.feignservice.contract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SeriesDto {
    private String id;
    private String seriesStatus;
    private String seriesName;
}
