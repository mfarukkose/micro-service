package com.mfaruk.kose.seriesservice.model.mySql;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "series")
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SeriesMySql extends BaseEntityModel implements Serializable {

    @Getter
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id")
    private String id;

    @Getter
    @Setter
    @Column(name = "series_name")
    private String seriesName;

    @Getter
    @Setter
    @ElementCollection
    @CollectionTable(name="Users", joinColumns=@JoinColumn(name="series_id"))
    @Column(name = "user")
    private List<String> users;

    @Getter
    @Setter
    @Column(name = "series_status")
    @Enumerated
    private SeriesStatus seriesStatus;

}
