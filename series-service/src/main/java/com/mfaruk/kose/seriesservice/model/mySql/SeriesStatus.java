package com.mfaruk.kose.seriesservice.model.mySql;

import lombok.Getter;

@Getter
public enum SeriesStatus {
    ACTIVE("Aktif"),
    PASSIVE("Pasif");

    private String label;

    SeriesStatus(String label) {
        this.label = label;
    }
}
