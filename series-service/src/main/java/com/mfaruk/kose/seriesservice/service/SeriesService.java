package com.mfaruk.kose.seriesservice.service;

import com.mfaruk.kose.seriesservice.dto.SeriesSaveDto;
import com.mfaruk.kose.feignservice.contract.SeriesDto;

public interface SeriesService {

    SeriesDto createSeries(SeriesSaveDto seriesSaveDto) throws Exception;
    SeriesDto getSeriesById(String id);
}
