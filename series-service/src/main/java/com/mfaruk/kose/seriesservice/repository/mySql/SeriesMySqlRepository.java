package com.mfaruk.kose.seriesservice.repository.mySql;

import com.mfaruk.kose.seriesservice.model.mySql.SeriesMySql;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeriesMySqlRepository extends JpaRepository<SeriesMySql, String> {
}
