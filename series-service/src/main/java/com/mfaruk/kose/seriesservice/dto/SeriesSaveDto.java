package com.mfaruk.kose.seriesservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.mfaruk.kose.seriesservice.model.mySql.SeriesStatus;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SeriesSaveDto {
    private String seriesName;
    private SeriesStatus seriesStatus;
}
