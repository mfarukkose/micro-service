package com.mfaruk.kose.seriesservice.repository.elasticSearch;

import com.mfaruk.kose.seriesservice.model.elasticSearch.SeriesElasticSearch;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeriesElasticSearchRepository extends ElasticsearchRepository<SeriesElasticSearch, String> {
}
