package com.mfaruk.kose.seriesservice.service.impl;

import com.mfaruk.kose.seriesservice.dto.SeriesSaveDto;
import com.mfaruk.kose.seriesservice.model.elasticSearch.SeriesElasticSearch;
import com.mfaruk.kose.seriesservice.model.mySql.SeriesMySql;
import com.mfaruk.kose.seriesservice.repository.elasticSearch.SeriesElasticSearchRepository;
import com.mfaruk.kose.seriesservice.repository.mySql.SeriesMySqlRepository;
import com.mfaruk.kose.seriesservice.service.NotificationService;
import com.mfaruk.kose.seriesservice.service.SeriesService;
import com.mfaruk.kose.feignservice.contract.SeriesDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SeriesServiceImpl implements SeriesService {

    @Autowired
    SeriesMySqlRepository seriesMySqlRepository;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    SeriesElasticSearchRepository seriesElasticSearchRepository;

    @Autowired
    NotificationService notificationService;

    @Transactional
    @Override
    public SeriesDto createSeries(SeriesSaveDto seriesSaveDto) throws Exception {
        try {
            SeriesMySql series = modelMapper.map(seriesSaveDto, SeriesMySql.class);
            seriesMySqlRepository.save(series);
            SeriesElasticSearch seriesElasticSearch = SeriesElasticSearch.builder()
                    .seriesName(series.getSeriesName())
                    .id(series.getId())
                    .seriesStatus(series.getSeriesStatus().getLabel())
                    .build();
            seriesElasticSearchRepository.save(seriesElasticSearch);
            notificationService.sendToQueue(series);
            return modelMapper.map(series, SeriesDto.class);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public SeriesDto getSeriesById(String id) {
        SeriesMySql series = seriesMySqlRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        return modelMapper.map(series, SeriesDto.class);
    }

}
