package com.mfaruk.kose.seriesservice.model.elasticSearch;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

@Data
@Builder
@EqualsAndHashCode(of = {"id"})
@Document(indexName = "series")
@AllArgsConstructor
@NoArgsConstructor
public class SeriesElasticSearch implements Serializable  {

    @Id
    private String id;
    private String seriesName;
    private String seriesStatus;
}
