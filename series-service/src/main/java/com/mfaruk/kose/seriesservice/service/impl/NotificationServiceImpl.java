package com.mfaruk.kose.seriesservice.service.impl;

import com.mfaruk.kose.feignservice.contract.NotificationModel;
import com.mfaruk.kose.seriesservice.model.mySql.SeriesMySql;
import com.mfaruk.kose.seriesservice.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@EnableBinding(Source.class)
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    private Source source;

    @Override
    public void sendToQueue(SeriesMySql seriesMySql) {
        NotificationModel notificationModel = new NotificationModel();
        notificationModel.setId(seriesMySql.getId());
        notificationModel.setSeriesName(seriesMySql.getSeriesName());
        try {
            source.output().send(MessageBuilder.withPayload(notificationModel).build());
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
