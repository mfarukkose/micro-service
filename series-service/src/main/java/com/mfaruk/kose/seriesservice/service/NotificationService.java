package com.mfaruk.kose.seriesservice.service;

import com.mfaruk.kose.seriesservice.model.mySql.SeriesMySql;

public interface NotificationService {

    void sendToQueue(SeriesMySql seriesMySql);
}
