package com.mfaruk.kose.seriesservice.controller;

import com.mfaruk.kose.seriesservice.dto.SeriesSaveDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface SeriesController {

    @RequestMapping(value = "/getSeriesById", method = RequestMethod.GET)
    @ResponseBody
    ResponseEntity<Object> getSeriesById(@RequestParam("id") String id);

    @RequestMapping(value = "/createSeries", method = RequestMethod.POST)
    @ResponseBody
    ResponseEntity<Object> createSeries(@RequestBody SeriesSaveDto seriesSaveDto);
}
