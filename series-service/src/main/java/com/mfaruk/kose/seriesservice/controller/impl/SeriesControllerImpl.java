package com.mfaruk.kose.seriesservice.controller.impl;

import com.mfaruk.kose.seriesservice.controller.SeriesController;
import com.mfaruk.kose.seriesservice.dto.SeriesSaveDto;
import com.mfaruk.kose.seriesservice.service.SeriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("series")
public class SeriesControllerImpl implements SeriesController {

    @Autowired
    SeriesService seriesService;

    @Override
    public ResponseEntity<Object> getSeriesById(String id) {
        try {
            return ResponseEntity.ok(seriesService.getSeriesById(id));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

    @Override
    public ResponseEntity<Object> createSeries(SeriesSaveDto seriesSaveDto) {
        try {
            return ResponseEntity.ok(seriesService.createSeries(seriesSaveDto ));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }
}
