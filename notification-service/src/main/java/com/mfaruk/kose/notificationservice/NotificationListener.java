package com.mfaruk.kose.notificationservice;
import com.mfaruk.kose.feignservice.contract.NotificationModel;
import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

@Log4j2
@EnableBinding(Sink.class)
public class NotificationListener {

    @StreamListener(Sink.INPUT)
    public void onNotification(NotificationModel notificationModel) {
        log.info("notif ------> " + notificationModel.toString());
    }
}
